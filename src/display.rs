use sdl2::pixels::Color;

use crate::chip8;

const HEIGHT: usize = crate::HEIGHT;
const WIDTH: usize = crate::WIDTH;
const PALETTE_COUNT: usize = 16;

const PALETTES: [[Color;2];PALETTE_COUNT] = [
    [Color::RGB(0, 0, 0), Color::RGB(255, 255, 255)],
    [Color::RGB(255, 255, 255), Color::RGB(0, 0, 0)],
    [Color::RGB(69, 89, 78), Color::RGB(200, 232, 214)],
    [Color::RGB(34, 35, 35), Color::RGB(240, 246, 240)],
    [Color::RGB(62, 35, 44), Color::RGB(237, 246, 214)],
    [Color::RGB(34, 42, 61), Color::RGB(237, 242, 226)],
    [Color::RGB(13, 19, 42), Color::RGB(173, 195, 232)],
    [Color::RGB(33, 44, 40), Color::RGB(114, 164, 136)],
    [Color::RGB(54, 54, 54), Color::RGB(236, 236, 236)],
    [Color::RGB(40, 14, 11), Color::RGB(255, 236, 201)],
    [Color::RGB(93, 175, 167), Color::RGB(93, 66, 66)],
    [Color::RGB(43, 0, 0), Color::RGB(204, 14, 19)],
    [Color::RGB(250, 202, 184), Color::RGB(36, 34, 52)],
    [Color::RGB(65, 54, 82), Color::RGB(100, 147, 255)],
    [Color::RGB(28, 34, 28), Color::RGB(224, 201, 224)],
    [Color::RGB(180, 231, 239), Color::RGB(175, 85, 52)],
];

pub struct Display {
    pub screen: [bool; WIDTH*HEIGHT],
    pub palette: [Color;2], // [off color, on color]
    current_palatte: usize,
    pub updated: bool,
}

impl Display {
    pub fn new() -> Display {
        Display {
            screen: [false; WIDTH*HEIGHT],
            current_palatte: 0,
            palette: PALETTES[0],
            updated: false,
        }
    }

    pub fn clear_screen(&mut self) {
        self.updated = true;
        for pixel in &mut self.screen {
            *pixel = false;
        }
    }

    // Draw the sprite pointed to by the index_register with the given ::HEIGHT at the given (x,y) to the display
    pub fn draw(&mut self, chip8: &mut chip8::Chip8, x_coord: usize, y_coord: usize, height: usize) {
        self.updated = true;
        chip8.var_regs[0xF] = 0;
        for row in 0..height {
            let curr_y = y_coord+row;
            if curr_y >= HEIGHT { break; } // do not draw off bottom of screen
            // The top line of the sprite is the byte pointed to by the index register
            // The bytes after that correspond to the next lines of the sprite
            let sprite = chip8.ram[chip8.index_register as usize + row];
            for col in 0..8 {
                let curr_x = x_coord+col;
                if curr_x >= WIDTH { break; } // do not draw off right side of screen
                // if the current pixel is ON in the sprite
                if sprite & (1 << (7-col)) != 0 {
                    // if the pixel at (curr_x, curr_y) is also ON, turn it OFF
                    if self.screen[coords(curr_x, curr_y)] {
                        self.screen[coords(curr_x, curr_y)] = false;
                        chip8.var_regs[0xF] = 1;
                    } else {
                        self.screen[coords(curr_x, curr_y)] = true;
                    }
                }
            }
        }
    }

    pub fn swap_palette(&mut self) {
        self.current_palatte = (self.current_palatte + 1) % PALETTE_COUNT; 
        self.palette = PALETTES[self.current_palatte];
    }

}

// Convert the (x,y) coordinates to the correct position in the array
fn coords(x: usize, y: usize) -> usize {
    x + y*WIDTH
}
