/* Resource: https://tobiasvl.github.io/blog/write-a-chip-8-emulator/ */

use std::{
    fs,
    path::Path,
    env,
    time::Duration,
};

extern crate sdl2;
use sdl2::{
    rect::Rect,
    render::Canvas,
    event::Event,
    keyboard::Scancode,
    EventPump,
};

mod decode;
mod chip8;
mod display;
mod font;
mod audio;

const WIDTH: usize = 64;
const HEIGHT: usize = 32;
const SCALE: usize = 10;
const INITIAL_SPEED: u32 = 700;

const FONT_START: usize = 0x050;
const ROM_START: usize = 0x200;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Enter rom path as argument");
        return;
    }
    let path = Path::new(&args[1]);
    let sdl_context = sdl2::init().unwrap();
    let (mut canvas, mut event_pump) = sdl_setup(&sdl_context);
    let audio = audio::audio_setup(&sdl_context)
        .expect("Audio setup error");

    let mut chip8 = chip8::Chip8::new();
    let mut display = display::Display::new();
    let mut speed = INITIAL_SPEED; // Emulation speed in instructions/second

    load_font(&mut chip8.ram, font::FONT);

    load_rom(&mut chip8.ram, path);

    'main_loop: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'main_loop,
                Event::KeyDown { scancode: Some(Scancode::P), .. } =>  {
                    display.swap_palette();
                    update_canvas(&display, &mut canvas);
                }
                Event::KeyDown { scancode: Some(Scancode::O), .. } => reset(&mut chip8, &mut display, path),
                Event::KeyDown { scancode: Some(Scancode::Left), .. } => {
                    if speed > INITIAL_SPEED/3 { speed -= 50; }
                    println!("{}", speed);
                },
                Event::KeyDown { scancode: Some(Scancode::Right), .. } => {
                    if speed < INITIAL_SPEED*3 { speed += 50; }
                    println!("{}", speed);
                },
                Event::KeyDown { scancode: Some(key), repeat: false, .. } => {
                    if let Some(x) = keymap(key) {
                        chip8.keypad[x] = true;
                    }
                }
                Event::KeyUp { scancode: Some(key), repeat: false, .. } => {
                    if let Some(x) = keymap(key) {
                        chip8.key_up(x);
                    }
                }
                _ => {}
            }
        }
        for _i in 0..speed/60 {
            let instruction = chip8.fetch();
            // println!("{:X}", instruction);
            decode::decode(instruction, &mut chip8, &mut display);
        }
        if display.updated {
            display.updated = false;
            update_canvas(&display, &mut canvas);
            canvas.present();
        }
        chip8.decrement_timers();
        if chip8.sound == 0 {
            audio.pause();
        } else {
            audio.resume();
        }

        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }

    // println!("{:X?}", chip8.ram);
    // println!("{:?}", chip8.display);

}

fn sdl_setup(sdl_context: &sdl2::Sdl) -> (Canvas<sdl2::video::Window>, EventPump){
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Chip-8 Emulator", (WIDTH*SCALE) as u32, (HEIGHT*SCALE) as u32)
        .build()
        .unwrap();

    let canvas = window.into_canvas()
        .present_vsync()
        .build()
        .unwrap();

    let event_pump = sdl_context.event_pump().unwrap();
    (canvas, event_pump)
}

// Load font into ram
fn load_font(ram: &mut [u8], font: [u8;80]) {
    for (i, byte) in font.iter().enumerate() {
        ram[FONT_START+i] = *byte;
    }
}

// Load rom into ram
fn load_rom(ram: &mut [u8], path: &Path) {
    let rom = fs::read(path)
        .expect("Failed to open file");
    for (i, byte) in rom.iter().enumerate() {
        ram[ROM_START+i] = *byte;
    }
}

fn update_canvas(display: &display::Display, canvas: &mut Canvas<sdl2::video::Window>) {
    canvas.set_draw_color(display.palette[0]);
    canvas.clear();
    canvas.set_draw_color(display.palette[1]);
    for (i, pixel) in display.screen.iter().enumerate() {
        if *pixel {
            let x = ((i % WIDTH) * SCALE) as i32;
            let y = ((i / WIDTH) * SCALE) as i32;
            canvas.fill_rect(Rect::new(x, y, SCALE as u32, SCALE as u32))
                .expect("Error Drawing to Canvas");
        }
    }
}

fn reset(chip8: &mut chip8::Chip8, display: &mut display::Display, rom_path: &Path) {
    *chip8 = chip8::Chip8::new();
    *display = display::Display::new();
    load_rom(&mut chip8.ram, &rom_path);
}

fn keymap(key: Scancode) -> Option<usize> {
    match key {
        Scancode::Num1 => Some(1),
        Scancode::Num2 => Some(2),
        Scancode::Num3 => Some(3),
        Scancode::Num4 => Some(0xC),
        Scancode::Q => Some(4),
        Scancode::W => Some(5),
        Scancode::E => Some(6),
        Scancode::R => Some(0xD),
        Scancode::A => Some(7),
        Scancode::S => Some(8),
        Scancode::D => Some(9),
        Scancode::F => Some(0xE),
        Scancode::Z => Some(0xA),
        Scancode::X => Some(0),
        Scancode::C => Some(0xB),
        Scancode::V => Some(0xF),
        _ => None,
    }
}
