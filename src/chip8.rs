pub struct Chip8 {
    pub ram: [u8;4096], // 4kb of ram
    pub pc: usize, // Program Counter
    pub index_register: u16,
    pub stack: Vec<usize>,
    pub delay: u8, // timer decremented every second if above 0
    pub sound: u8, // same as delay but plays a sound when above 0
    pub var_regs: Vec<u8>,
    pub keypad: [bool; 16],
    pub wait: WaitState,
}

pub enum WaitState {
    Released(u8),
    Waiting,
    None,
}

impl Chip8 {
    pub fn new() -> Chip8 {
        Chip8 {
            ram: [0;4096],
            pc: crate::ROM_START,
            index_register: 0,
            stack: Vec::new(),
            delay: 0,
            sound: 0,
            var_regs: vec![0;16],
            keypad: [false; 16],
            wait: WaitState::None,
        }
    }

    pub fn decrement_timers(&mut self) {
        if self.delay > 0 {
            self.delay -= 1;
        }
        if self.sound > 0 {
            self.sound -= 1;
        }
    }

    // fetch next instruction and increment program counter
    // instructions are 16 bits, the bytes at pc and pc+1
    pub fn fetch(&mut self) -> u16 {
        let byte1 = self.ram[self.pc] as u16;
        let byte2 = self.ram[self.pc+1] as u16;
        self.pc += 2;
        // join the bytes together, first followed by the second
        byte1 << 8 | byte2
    }

    pub fn key_up(&mut self, key: usize) {
        match self.wait {
            WaitState::Waiting => {
                self.wait = WaitState::Released(key as u8);
            }
            _ => {}
        }
        self.keypad[key] = false;
    }
}
