use rand::Rng;
use crate::chip8;
use crate::display;

pub fn parse_instruction(instruction: u16) -> (usize, usize, usize, usize, u8, u16) {
    let mut nibbles = vec![0;4];
    for position in 0..4 {
        let shift = 12 - 4 * position;
        let mask = 0xF << shift;
        nibbles[position] = ((instruction & mask) >> shift) as usize;
    }
    let nn = (instruction & 0x00FF) as u8;
    let nnn = instruction & 0x0FFF;
    (nibbles[0], nibbles[1], nibbles[2], nibbles[3], nn, nnn)
}

pub fn decode(instruction: u16, chip8: &mut chip8::Chip8, display: &mut display::Display) {
    let (op, x, y, n, nn, nnn) = parse_instruction(instruction);

    match op {
        0x0 => {
            match nnn {
                // (00E0) Clear screen
                0x0E0 => display.clear_screen(),
                // (00EE) return from subroutine
                0x0EE => chip8.pc = chip8.stack.pop()
                    .expect("(00EE) Returned from function with no callers on stack"),
                _ => println!("Unknown Instruction: {:X}{:X}{:X}{:X}", op, x, y, n),
            }
        },
        0x1 => {
            // (1NNN) Jump
            chip8.pc = nnn as usize;
        }
        0x2 => {
            //(2NNN) Jump to subroutine
            chip8.stack.push(chip8.pc);
            chip8.pc = nnn as usize;
        }
        0x3 => if chip8.var_regs[x] == nn { chip8.pc += 2; }   // (3XNN) Skip if VX == nn
        0x4 => if chip8.var_regs[x] != nn { chip8.pc += 2; }   // (4XNN) Skip if VX != nn
        0x5 => if chip8.var_regs[x] == chip8.var_regs[y] { chip8.pc += 2; }   // (5XY0) Skip if VX == XY
        0x6 => {
            // (6XNN) Set register VX to NN
            chip8.var_regs[x] = nn;
        }
        0x7 => {
            // (7XNN) Add nn to register VX
            chip8.var_regs[x] = chip8.var_regs[x].wrapping_add(nn);
        }
        0x8 => handle_op8(chip8, x, y, n),
        0x9 => if chip8.var_regs[x] != chip8.var_regs[y] { chip8.pc += 2; }   // (9XY0) Skip if VX != XY
        0xA => {
            // (ANNN) Set index register to NNN
            chip8.index_register = nnn;
        }
        0xB => {
            // (BNNN) Jump to NNN + V0
            chip8.pc = nnn as usize + chip8.var_regs[0] as usize;
        }
        0xC => {
            // (CXNN) Set VX to a random number ANDed with NN
            chip8.var_regs[x] = rand::thread_rng().gen::<u8>() & nn;
        }
        0xD => {
            // (DXYN) Display
            let x_coord = chip8.var_regs[x] % crate::WIDTH as u8;
            let y_coord = chip8.var_regs[y] % crate::HEIGHT as u8;
            display.draw(chip8, x_coord.into(), y_coord.into(), n);
        }
        0xE => {
            match nn {
                0x9E => {
                    // (EX9E) Skip if the key in VX is pressed
                    if chip8.var_regs[x] > 16 {
                        println!("Invalid key value in EX9E: {}", chip8.var_regs[x]);
                        return;
                    }
                    if chip8.keypad[chip8.var_regs[x] as usize] {
                        chip8.pc += 2;
                    }
                }
                0xA1 => {
                    // (EXA1) Skip if the key in VX is not pressed
                    if chip8.var_regs[x] > 16 {
                        println!("Invalid key value in EXA1: {}", chip8.var_regs[x]);
                        return;
                    }
                    if !chip8.keypad[chip8.var_regs[x] as usize] {
                        chip8.pc += 2;
                    }
                }
                _ => {
                    println!("Unknown Instruction: {:X}{:X}{:X}{:X}", op, x, y, n);
                }
            }
        }
        0xF => handle_opF(chip8, x, nn),
        _ => {
            println!("Unknown Instruction: {:X}{:X}{:X}{:X}", op, x, y, n);
        }
    }
}

// decode and execute instructions that begin with 8 (Logic and Arithmetic)
fn handle_op8(chip8: &mut chip8::Chip8, x: usize, y: usize, n: usize) {
    match n {
        0x0 => chip8.var_regs[x] = chip8.var_regs[y], // (8XY0) set VX to VY
        0x1 => chip8.var_regs[x] |= chip8.var_regs[y], // (8XY1) Logical OR
        0x2 => chip8.var_regs[x] &= chip8.var_regs[y], // (8XY2) Logical AND
        0x3 => chip8.var_regs[x] ^= chip8.var_regs[y], // (8XY3) Logical XOR
        0x4 => {
            // (8XY4) VX = VX + VY, set VF to 1 if there is overflow
            let check = chip8.var_regs[x] as u16 + chip8.var_regs[y] as u16 > 255;
            chip8.var_regs[0xF] = if check { 1 } else { 0 };
            chip8.var_regs[x] = chip8.var_regs[x].wrapping_add(chip8.var_regs[y]);
        }
        0x5 => {
            // (8XY4) VX = VX - VY, set VF to 1 if there is underflow
            let check = chip8.var_regs[x] > chip8.var_regs[y];
            chip8.var_regs[0xF] = if check { 1 } else { 0 };
            chip8.var_regs[x] = chip8.var_regs[x].wrapping_sub(chip8.var_regs[y]);
        }
        0x7 => {
            // (8XY7) VX = VY - VX, set VF to 1 if there is underflow
            let check = chip8.var_regs[y] > chip8.var_regs[x];
            chip8.var_regs[0xF] = if check { 1 } else { 0 };
            chip8.var_regs[x] = chip8.var_regs[y].wrapping_sub(chip8.var_regs[x]);
        }
        0x6 => {
            // (8XY6) Right Bitshift VX, put lost bit in VF
            chip8.var_regs[0xF] = chip8.var_regs[x] & 1;
            chip8.var_regs[x] >>= 1;
        }
        0xE => {
            // (8XYE) Left Bitshift VX, put lost bit in VF
            chip8.var_regs[0xF] = (chip8.var_regs[x] & 1<<7) >> 7;
            chip8.var_regs[x] <<= 1;
        }
        _ => {
            println!("Unknown Instruction: {:X}{:X}{:X}{:X}", 8, x, y, n);
        }
    }
}

#[allow(non_snake_case)]
fn handle_opF(chip8: &mut chip8::Chip8, x: usize, nn: u8) {
    match nn {
        0x07 => chip8.var_regs[x] = chip8.delay, // (FX07) Set VX to the value in the delay timer
        0x15 => chip8.delay = chip8.var_regs[x], // (FX15) Set delay timer to the value in VX
        0x18 => chip8.sound = chip8.var_regs[x], // (FX18) Set sound timer to the value in VX
        0x1E => {
            // (FX1E) Add VX to the index register
            chip8.index_register += chip8.var_regs[x] as u16;
            chip8.var_regs[0xF] = if chip8.index_register > 0x0FFF { 1 } else { 0 };
        }
        0x0A => {
            // (FX0A) Wait for input, then put key into VX
            match chip8.wait {
                chip8::WaitState::Released(key) => {
                    chip8.var_regs[x] = key;
                    chip8.wait = chip8::WaitState::None;
                }
                chip8::WaitState::Waiting => {
                    chip8.pc -= 2;
                }
                chip8::WaitState::None => {
                    chip8.wait = chip8::WaitState::Waiting;
                    chip8.pc -= 2;
                }
            }
        }
        0x29 => {
            // (FX29) set index register to font character in the last 4 bits of VX
            let character = (chip8.var_regs[x] & 0x0F) as u16;
            chip8.index_register = crate::FONT_START as u16 + character * 5; // Each font character is 5 bytes long 
        }
        0x33 => {
            // (FX33) Convert the value in VX to its 3 decimal digits and store them at the index_register
            let number = chip8.var_regs[x];
            for i in (0..3).rev() {
                chip8.ram[(chip8.index_register + (2-i)) as usize] = (number / 10u8.pow(i as u32)) % 10;
            }
        }
        0x55 => {
            // (FX55) Store values in V0 through VX (inclusive) in ram starting at the index register
            for i in 0..x+1 {
                chip8.ram[chip8.index_register as usize + i] = chip8.var_regs[i];
            }
        }
        0x65 => {
            // (FX56) Load values from ram starting at the index register into V0 through VX (inclusive)
            for i in 0..x+1 {
                chip8.var_regs[i] = chip8.ram[chip8.index_register as usize + i];
            }
        }
        _ => {
            println!("Unknown Instruction: {:X}{:X}{:X}", 0xF, x, nn);
        }
    }
}
